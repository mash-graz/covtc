FROM registry.gitlab.com/mash-graz/ffmpeg-svt

COPY target/release/covtc /usr/local/bin

ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu
