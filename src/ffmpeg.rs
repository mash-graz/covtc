use crate::exec_serializer::ExecSerializer;
use anyhow::{anyhow, Context, Result};
//use std::collections::HashMap;
use std::fs::create_dir_all;
//use std::io::prelude::*;
//use std::io::BufReader;
//use std::net::TcpListener;
use std::path::Path;
use std::process::{Command, ExitStatus};
//use std::thread;
use std::time::SystemTime;

use crate::pipeline::{AudioCodec, Codec, Input, Pipeline, VideoCodec};

/// This modul represents on actual realization of the transcoding pipeline by utilizing FFmpeg.

pub struct FFmpeg<'a> {
    p: &'a Pipeline,
    parts: Vec<String>,
}

impl<'a> ExecSerializer<'a> for FFmpeg<'a> {
    fn new(p: &'a Pipeline) -> FFmpeg<'a> {
        let mut ff = FFmpeg {
            p: p,
            parts: Vec::new(),
        };
        ff.parts.append(&mut ff.input_section().unwrap());
        ff.parts.append(&mut ff.output_section().unwrap());
        ff
    }

    fn to_string(&self) -> Result<String> {
        Ok("ffmpeg ".to_owned() + &self.parts.join(" "))
    }

    fn exec(&self) -> Result<ExitStatus> {
        // check existence of target dir / files
        if self.p.output.dash_hls.is_some() {
            let base_dir = Path::new(&self.p.output.dash_hls.as_ref().unwrap().base_dir);
            if base_dir.exists() {
                return Err(anyhow!("Directory already exists: {:?}", base_dir));
            } else {
                create_dir_all(base_dir.join("dash_hls")).context("create dash_hls directory")?;
            }
        }

        // progress display
        // let handler = thread::spawn(|| {
        //     let mut line = String::new();
        //     let mut map = HashMap::new();
        //     let listener = TcpListener::bind("127.0.0.1:30000").unwrap();
        //     let (stream, _addr) = listener.accept().unwrap();
        //     let mut reader = BufReader::new(stream);
        //     loop {
        //         let n = reader.read_line(&mut line).unwrap();
        //         let parts = line.split("=").collect::<Vec<&str>>();
        //         // println!("parts: {}, len: {}, {:?}", parts.len(), n, parts);
        //         if n == 0 {
        //             break;
        //         }
        //         if parts.len() == 2 {
        //             map.insert(parts[0].to_string(), parts[1].trim().to_string());
        //         }
        //         if parts[0] == "progress" {
        //             print!(
        //                 "  frame: {:6} fps: {} ({})    \r",
        //                 map.get("frame").unwrap(),
        //                 map.get("fps").unwrap(),
        //                 map.get("speed").unwrap(),
        //             );
        //             std::io::stdout().flush().unwrap();
        //         }
        //         line.clear();
        //     }
        // });

        let start_time = SystemTime::now();
        let res = Command::new("ffmpeg")
            .arg("-y") // silently overwrite files
            .arg("-hide_banner")
            //.arg("-nostats")
            //.arg("-loglevel")
            //.arg("level+warning")
            //.arg("-vstats_file")
            //.arg("/tmp/vstats")
            //.arg("-progress")
            //.arg("tcp://localhost:30000")
            .args(&self.parts)
            .status()
            .context("ffmpeg execution");
        println!("\ntime elapsed: {:?}", start_time.elapsed().unwrap());
        // handler.join().unwrap();
        res
    }
}

impl<'a> FFmpeg<'a> {
    /// translate the input section of the configuration
    fn input_section(&'a self) -> Result<Vec<String>> {
        let mut parts: Vec<String> = Vec::new();

        match self.p.input {
            // FILE source
            Input::inputfile(ref i) => {
                if i.realtime {
                    parts.push("-re".to_string());
                }

                if i.seek.is_some() {
                    parts.push("-ss".to_string());
                    parts.push(i.seek.unwrap().to_string());
                }

                parts.push("-i".to_string());
                if i.path.is_some() {
                    parts.push(i.path.as_ref().unwrap().to_string());
                } else {
                    parts.push("-".to_string());
                }

                if i.duration.is_some() {
                    parts.push("-t".to_string());
                    parts.push(i.duration.unwrap().to_string());
                }
            }

            // TESTSRC
            Input::testsrc(ref ts) => {
                parts.push("-f".to_string());
                parts.push("lavfi".to_string());
                parts.push("-i".to_string());

                if ts.duration.is_some() {
                    parts.push(format!(
                        "testsrc=s={}:r={}:d={}[out0],sine=f=440:b=2:d={}[out1]",
                        ts.resolution,
                        ts.framerate,
                        ts.duration.unwrap(),
                        ts.duration.unwrap()
                    ));
                } else {
                    parts.push(format!(
                        "testsrc=s={}:r={}[out0],sine=f=440:b=2[out1]",
                        ts.resolution, ts.framerate
                    ));
                }
            }

            // LISTENER
            Input::listener(ref l) => {
                parts.push("-listen".to_string());
                parts.push("1".to_string());
                parts.push("-i".to_string());
                parts.push(l.url.to_owned());
            }
        };

        Ok(parts)
    }

    /// translate the output section of the configuration
    fn output_section(&'a self) -> Result<Vec<String>> {
        let mut parts: Vec<String> = Vec::new();

        let mut video_counter: u32 = 0;
        let mut audio_counter: u32 = 0;

        if self.p.output.dash_hls.is_some() {
            // general dash_hls encoding options
            parts.push("-force_key_frames".to_string());
            parts.push(format!(
                "expr:gte(t,n_forced*{})",
                &self.p.output.dash_hls.as_ref().unwrap().keyframe_interval
            ));

            parts.push("-pix_fmt".to_string());
            parts.push("yuv420p".to_string());

            for codec_group in self.p.output.dash_hls.as_ref().unwrap().codec_list.iter() {
                for variant in codec_group.variantes.iter() {
                    match codec_group.codec {
                        // VIDEO tracks
                        Codec::VideoCodec(ref codec) => {
                            // video codec handling
                            parts.push("-map".to_string());
                            parts.push("v:0".to_string());
                            parts.push(format!("-c:v:{}", video_counter));
                            parts.push(format!("{:?}", &codec));

                            if *codec == VideoCodec::libx264 || *codec == VideoCodec::libx265 {
                                parts.push(format!("-preset:v:{}", video_counter));
                                parts.push("faster".to_string());
                            } else if *codec == VideoCodec::vp9 {
                                parts.push(format!("-row-mt:v:{}", video_counter));
                                parts.push("1".to_string());
                            }

                            // video bitrate handling
                            if variant.crf.is_some() && variant.bitrate.is_some() {
                                let limit =
                                    (variant.bitrate.unwrap() as f32 * 1.1).round().to_string();
                                // we still have to set the constant rate to get
                                // correct bandwidth entries in the manifests
                                parts.push(format!("-b:v:{}", video_counter));
                                parts.push(variant.bitrate.unwrap().to_string());
                                parts.push(format!("-crf:v:{}", video_counter));
                                parts.push(variant.crf.unwrap().to_string());
                                parts.push(format!("-bufsize:v:{}", video_counter));
                                parts.push(variant.bitrate.unwrap().to_string());
                                parts.push(format!("-maxrate:v:{}", video_counter));
                                parts.push(limit.to_owned());
                            } else if variant.crf.is_some() {
                                parts.push(format!("-crf:v:{}", video_counter));
                                parts.push(variant.crf.unwrap().to_string());
                            } else if variant.bitrate.is_some() {
                                let limit =
                                    (variant.bitrate.unwrap() as f32 * 1.1).round().to_string();
                                parts.push(format!("-b:v:{}", video_counter));
                                parts.push(variant.bitrate.unwrap().to_string());
                                parts.push(format!("-bufsize:v:{}", video_counter));
                                parts.push(variant.bitrate.unwrap().to_string());
                                parts.push(format!("-maxrate:v:{}", video_counter));
                                parts.push(limit.to_owned());
                            }

                            // image scaling
                            if variant.width.is_some() && variant.height.is_some() {
                                parts.push(format!("-filter:v:{}", video_counter));
                                parts.push(format!(
                                    "scale={}x{}",
                                    &variant.width.unwrap(),
                                    &variant.height.unwrap()
                                ));
                            } else if variant.width.is_some() {
                                parts.push(format!("-filter:v:{}", video_counter));
                                parts.push(format!("scale=w={}:h=-8", &variant.width.unwrap()));
                            } else if variant.height.is_some() {
                                parts.push(format!("-filter:v:{}", video_counter));
                                parts.push(format!("scale=w=-8:h={}", &variant.height.unwrap()));
                            }
                            video_counter += 1;
                        }

                        // AUDIO tracks
                        Codec::AudioCodec(ref codec) => {
                            // audio codec handling
                            parts.push("-map".to_string());
                            parts.push("a:0".to_string());
                            if *codec == AudioCodec::audio_passtrough {
                                parts.push(format!("-c:a:{}", audio_counter));
                                parts.push("copy".to_string());
                            } else {
                                parts.push(format!("-c:a:{}", audio_counter));
                                parts.push(format!("{:?}", &codec));

                                // audio bandwith handling
                                if variant.bitrate.is_some() {
                                    parts.push(format!("-b:a:{}", audio_counter));
                                    parts.push(variant.bitrate.unwrap().to_string());
                                }

                                if variant.r128 {
                                    parts.push(format!("-filter:a:{}", audio_counter));
                                    parts.push("loudnorm".to_string());
                                }
                            }
                            audio_counter += 1;
                        }
                    }
                }
            }

            // dash finalize
            parts.push("-format_options".to_string());
            parts.push("movflags=cmaf".to_string());

            parts.push("-seg_duration".to_string());
            parts.push(
                self.p
                    .output
                    .dash_hls
                    .as_ref()
                    .unwrap()
                    .segment_duration
                    .to_string(),
            );

            parts.push("-frag_duration".to_string());
            parts.push(
                self.p
                    .output
                    .dash_hls
                    .as_ref()
                    .unwrap()
                    .keyframe_interval
                    .to_string(),
            );

            if self.p.output.dash_hls.as_ref().unwrap().single_file {
                parts.push("-single_file".to_string());
                parts.push("1".to_string());
            }

            parts.push("-adaptation_sets".to_string());
            parts.push("id=0,streams=v id=1,streams=a".to_string());

            parts.push("-hls_playlist".to_string());
            parts.push("1".to_string());

            parts.push("-f".to_string());
            parts.push("dash".to_string());

            // final output location (dash manifest / file / URL)
            parts.push(
                Path::new(&self.p.output.dash_hls.as_ref().unwrap().base_dir)
                    .join("dash_hls")
                    .join("manifest.mpd")
                    .to_str()
                    .unwrap()
                    .to_owned(),
            );
        }

        // Auxiliary recording and forwarding
        if self.p.output.auxiliary.is_some() {
            for aux in self.p.output.auxiliary.as_ref().unwrap().iter() {
                parts.push("-map".to_string());
                parts.push("v:0".to_string());
                parts.push("-map".to_string());
                parts.push("a:0".to_string());
                parts.push(format!("-c:v:{}", video_counter));
                parts.push("copy".to_string());
                parts.push(format!("-c:a:{}", video_counter));
                parts.push("copy".to_string());

                parts.push("-f".to_string());
                parts.push(format!("{:?}", aux.container));

                parts.push(aux.path.to_owned());

                video_counter += 1;
                audio_counter += 1;
            }
        }

        Ok(parts)
    }
}

#[test]
fn fileinput_test() {
    let p: Pipeline = crate::parse_yaml_config_string(
        "
---
input:
  inputfile:
    path: https://media.xiph.org/tearsofsteel/tears_of_steel_1080p.webm
    realtime: true
    seek: 5
    duration: 15
output:
  auxiliary:
    - path: /tmp/testsrc.webm
      container: webm
",
    )
    .unwrap();
    assert_eq!(
        FFmpeg::new(&p).to_string().unwrap(),
        "ffmpeg -re -ss 5 -i https://media.xiph.org/tearsofsteel/tears_of_steel_1080p.webm \
        -t 15 -map v:0 -map a:0 -c:v:0 copy -c:a:0 copy -f webm /tmp/testsrc.webm"
    );
    assert!(FFmpeg::new(&p).exec().unwrap().success());
}

#[test]
fn testsrc_test() {
    let p: Pipeline = crate::parse_yaml_config_string(
        "
---
input:
  testsrc:
    resolution: 1280x720
    duration: 10
output:
  auxiliary:
    - path: /dev/null
      container: null
",
    )
    .unwrap();
    assert_eq!(
        FFmpeg::new(&p).to_string().unwrap(),
        "ffmpeg -f lavfi -i testsrc=s=1280x720:r=25:d=10[out0],sine=f=440:b=2:d=10[out1] \
        -map v:0 -map a:0 -c:v:0 copy -c:a:0 copy -f null /dev/null"
    );
    assert!(FFmpeg::new(&p).exec().unwrap().success());
}

#[test]
fn listen_test() {
    let p: Pipeline = crate::parse_yaml_config_string(
        "
---
input:
  listener:
    url: rtmp://example.org/live/test
output: {}
",
    )
    .unwrap();
    assert_eq!(
        FFmpeg::new(&p).to_string().unwrap(),
        "ffmpeg -listen 1 -i rtmp://example.org/live/test"
    );
}

#[test]
fn dash() {
    let p: Pipeline = crate::parse_yaml_config_string(
        "
---
input:
  inputfile: 
    path: https://media.xiph.org/tearsofsteel/tears_of_steel_1080p.webm
    realtime: true
    seek: 50
    duration: 15
output:
  dash_hls: 
    base_dir: /tmp/covtc_output
    # single_file: true
    codec_list: 
      - codec: libx264
        variantes:
          # - crf: 23
          - bitrate: 8000000
            height: 1080
          # - crf: 23
          - bitrate: 4000000
            height: 720
      - codec: vp9
        variantes:
          - bitrate: 9000000
      - codec: aac
        variantes:
          - bitrate: 192000
            r128: true
    ",
    )
    .unwrap();
    assert_eq!(
        FFmpeg::new(&p).to_string().unwrap(),
        "ffmpeg -re -ss 50 -i https://media.xiph.org/tearsofsteel/tears_of_steel_1080p.webm \
        -t 15 -force_key_frames expr:gte(t,n_forced*2) -pix_fmt yuv420p \
        -map v:0 -c:v:0 libx264 -preset:v:0 faster -b:v:0 8000000 -bufsize:v:0 8000000 \
        -maxrate:v:0 8800000 -filter:v:0 scale=w=-2:h=1080 \
        -map v:0 -c:v:1 libx264 -preset:v:1 faster -b:v:1 4000000 -bufsize:v:1 4000000 \
        -maxrate:v:1 4400000 -filter:v:1 scale=w=-2:h=720 \
        -map v:0 -c:v:2 vp9 -row-mt:v:2 1 -b:v:2 9000000 -bufsize:v:2 9000000 \
        -maxrate:v:2 9900000 -map a:0 -c:a:0 aac -b:a:0 192000 -filter:a:0 loudnorm \
        -format_options movflags=cmaf -seg_duration 6 -frag_duration 2 -adaptation_sets \
        id=0,streams=v id=1,streams=a -hls_playlist 1 -f dash /tmp/covtc_output/dash_hls/manifest.mpd"
    );
    assert!(FFmpeg::new(&p).exec().unwrap().success());
    std::fs::remove_dir_all("/tmp/covtc_output").unwrap();
}
