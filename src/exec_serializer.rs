use crate::pipeline::Pipeline;
use anyhow::Result;
use std::process::ExitStatus;

/// the execution serializer defines a common interface for different actual
/// realizations of the transcoding pipeline (e.g. for ffmpeg or gstreamer)

pub trait ExecSerializer<'a> {
    /// Initialize the execution serializer
    fn new(p: &'a Pipeline) -> Self;

    /// return a command string for manual execution
    /// of the underlying transcoder (e.g. ffmpeg)
    fn to_string(&self) -> Result<String>;

    /// start the execution
    fn exec(&self) -> Result<ExitStatus>;
}
