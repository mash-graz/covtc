use anyhow::{anyhow, Result};
/// # prepare included html page and web player
use minreq;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use tar::Archive;

/// download and unpack the indigo player
/// at the specified location
fn download_player(path: &str) -> Result<()> {
    let response = minreq::get(
        "https://gitlab.com/mash-graz/indigo-player/-/archive/master/indigo-player-ci-master.tar?path=lib")
        .with_timeout(30)
        .send()?;

    if response.status_code != 200 {
        return Err(anyhow!("web player download: {}", response.reason_phrase));
    } else if response.headers["content-length"] == "0" {
        return Err(anyhow!("web player download: empty download"));
    }

    Archive::new(response.as_bytes()).unpack(path)?;

    Ok(())
}

/// write the `index.html` file
///
/// this is just a temporary placeholder for a
/// more suitable template based solution
fn index_html(base_dir: &str) -> Result<()> {
    let mut file = File::create(Path::new(base_dir).join("index.html"))?;
    file.write_all(
        br#"
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>covtc video content</title>
  </head> 

  <body>
    <div id="playerContainer"></div>
    <script src="indigo-player-master-lib/lib/indigo-player.js"></script>

    <script>

      const config = {
        sources: [
            {
                type: 'dash',
                src: 'dash_hls/manifest.mpd',
            },
            {
                type: 'hls',
                src: 'dash_hls/master.m3u8',
            },
        ],
      };

      const element = document.getElementById('playerContainer');
      const player = IndigoPlayer.init(element, config);
    </script>

    <p><small>DASH/HLS video delivery realized by 
      <a href="https://gitlab.com/mash-graz/covtc">covtc</a> for
      <a href="http://mur.at">mur.at</a></small>
    </p>
  </body>
</html>
        "#,
    )?;
    Ok(())
}

/// download and install the web player and an `index.html` file
pub fn install_player(base_dir: &str) -> Result<()> {
    download_player(base_dir)?;
    index_html(base_dir)?;
    Ok(())
}

#[test]
fn install_player_test() {
    install_player("/tmp/covtc_output").unwrap();
    fs::remove_dir_all("/tmp/covtc_output").unwrap();
}
