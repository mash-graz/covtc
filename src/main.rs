use std::path::PathBuf;
use structopt::StructOpt;

use schemars::schema_for;
use serde_any;
use serde_json;
use serde_yaml;

use std::default::Default;

use std::fs::File;

use anyhow::{anyhow, Context, Result};
use env_logger;
use log::debug;

mod pipeline;
use pipeline::Pipeline;

mod exec_serializer;
mod ffmpeg;
mod player;

use crate::exec_serializer::ExecSerializer;
use ffmpeg::FFmpeg;
use player::install_player;

/// parse Json configuration string
pub fn parse_json_config_string(conf: &str) -> Result<Pipeline> {
    serde_json::from_str(conf).context("parsing Json config string")
}

/// parse YAML configuration string
pub fn parse_yaml_config_string(conf: &str) -> Result<Pipeline> {
    serde_yaml::from_str(conf).context("parsing YAML config string")
}

#[test]
fn minimal_json_test() {
    let p: Result<Pipeline> = parse_json_config_string(
        r#"
{"input":{"inputfile":{"path":"/tmp/test"}},"output":{}}
"#,
    );
    assert!(p.is_ok());
}

#[test]
fn minimal_yaml_test() {
    let p: Result<Pipeline> = parse_yaml_config_string(
        "
---
input:
  inputfile:
    path: /tmp/test
output: {}
",
    );
    assert!(p.is_ok());
}

/// read YAML or Json configuration file  
/// the file type is simply guessed by the filename suffix
pub fn read_config(filename: &PathBuf) -> Result<Pipeline> {
    let res = serde_any::from_file::<Pipeline, _>(filename.to_str().unwrap());
    match res {
        Ok(pipe) => Ok(pipe),
        Err(err) => Err(anyhow!("reading config File {:?}.\n{}", filename, err)),
    }
}

/// generate a Json Schema for the expected configuration data.
/// It will use `stdout` if no `filename` is given.
fn generate_schema(filename: &Option<PathBuf>) -> Result<()> {
    let schema = schema_for!(Pipeline);

    match filename {
        None => println!("{}", serde_json::to_string_pretty(&schema).unwrap()),
        Some(ref path) => {
            let buffer =
                File::create(path).context(format!("trying to write schema {:?}", path))?;
            serde_json::to_writer_pretty(&buffer, &schema).unwrap()
        }
    }
    Ok(())
}

/// generate an example of the expected YAML config file
/// It will use `stdout` if no `filename` is given.
fn generate_example(filename: &Option<PathBuf>) -> Result<()> {
    let example: Pipeline = Default::default();

    match filename {
        None => println!("{}", serde_yaml::to_string(&example).unwrap()),
        Some(ref path) => {
            let buffer =
                File::create(path).context(format!("trying to write example {:?}", path))?;
            serde_yaml::to_writer(&buffer, &example).unwrap()
        }
    }
    Ok(())
}

/// # The Command Line Options of the application
#[derive(Debug, StructOpt)]
#[structopt()]
struct Opt {
    /// dumps the Json Schema.  
    /// (use 'stdin' if used without argument)
    #[structopt(short, long)]
    #[allow(clippy::option_option)]
    schema: Option<Option<PathBuf>>,

    /// write a YAML example file.  
    /// (use 'stdin' if used without arguments)
    #[structopt(short, long)]
    #[allow(clippy::option_option)]
    example: Option<Option<PathBuf>>,

    /// YAML or Json config file (appropriate suffix required)
    #[structopt(short, long)]
    #[allow(clippy::option_option)]
    config: Option<PathBuf>,

    /// Json config string
    #[structopt(short, long, env = "COVTC_JSON_CONFIG")]
    #[allow(clippy::option_option)]
    json_config: Option<String>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::builder()
        .format_timestamp(None)
        .format_module_path(false)
        .init();

    let opt = Opt::from_args();
    debug!("{:#?}", opt);

    if opt.schema.is_some() {
        let arg = opt.schema.unwrap();
        generate_schema(&arg)?;
    }

    if opt.example.is_some() {
        let arg = opt.example.unwrap();
        generate_example(&arg)?;
    }

    let mut pipeline: Option<Pipeline> = None;
    if opt.config.is_some() {
        let arg = opt.config.unwrap();
        pipeline = Some(read_config(&arg)?);
    }

    if opt.json_config.is_some() {
        pipeline = Some(parse_json_config_string(&opt.json_config.unwrap())?);
    }

    if pipeline.is_some() {
        debug!("{:#?}", pipeline.as_ref().unwrap());

        let ff = FFmpeg::new(pipeline.as_ref().unwrap());
        debug!("serialized: {:#?}", ff.to_string()?);
        let res = ff.exec()?;
        if res.success() && pipeline.as_ref().unwrap().output.dash_hls.is_some() {
            install_player(
                &pipeline
                    .as_ref()
                    .unwrap()
                    .output
                    .dash_hls
                    .as_ref()
                    .unwrap()
                    .base_dir,
            )?;
        }
    }
    Ok(())
}
