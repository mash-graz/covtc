use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::default::Default;

/// The abstract data tree of the general configuration

/// # Processing Pipeline
///
/// the main entry point of a transcoding pipeline  
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Pipeline {
    pub input: Input,
    pub output: Output,
}

/// the kind of video source
#[allow(non_camel_case_types)]
#[derive(JsonSchema, Serialize, Deserialize, Debug)]
pub enum Input {
    inputfile(InputFile),
    testsrc(TestSrc),
    listener(Listener),
}
impl Default for Input {
    fn default() -> Self {
        Input::inputfile(InputFile::default())
    }
}

/// using a File or `stdin` as video source
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct InputFile {
    /// path of input file (use 'stdin' if omitted)
    #[serde(default)]
    pub path: Option<String>,
    /// read in realtime
    #[serde(default = "default_as_false")]
    pub realtime: bool,
    /// seek to start position (in seconds)
    #[serde(default)]
    pub seek: Option<u32>,
    /// limit playtime (in seconds)
    #[serde(default)]
    pub duration: Option<u32>,
}

fn default_as_false() -> bool {
    false
}

/// testsource generator for encoding benchmarks etc.
#[derive(JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct TestSrc {
    /// resolution of test content (default: 1920x1080)
    #[serde(default = "default_resolution")]
    pub resolution: String,

    ///framerate of test content (default: 25fps)
    #[serde(default = "default_framerate")]
    pub framerate: u32,

    /// duration of the generated test content
    #[serde(default)]
    pub duration: Option<u32>,
}

fn default_resolution() -> String {
    "1920x1080".to_owned()
}

fn default_framerate() -> u32 {
    25
}

/// listen to network connections
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Listener {
    /// URL specifying the network protocol and listening port
    pub url: String,
}

/// the block containing all output targets of the transcoding pipeline
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Output {
    /// the adadptive dash_hls targets
    pub dash_hls: Option<DashHls>,
    /// list of additiona auxilary output targets
    pub auxiliary: Option<Vec<Auxiliary>>,
}

// this block contains all the adaptive stream targets and the
// global configuration of the CMAF files resp. fragments
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct DashHls {
    /// keyframe interval in seconds (default: 2s)
    #[serde(default = "default_keyframe_interval")]
    pub keyframe_interval: u32,
    /// segment duration (default: 6s)
    #[serde(default = "default_segment_duration")]
    pub segment_duration: u32,
    /// use unsegmented files and offsets
    ///
    /// although this would be a nice feature,
    /// it doesn't work reliable in ffmpeg in it's
    /// present state.
    #[serde(default = "default_as_false")]
    pub single_file: bool,
    /// the location of DASH,HLS,Web-output
    /// default: ./dash_hls
    #[serde(default = "default_dash_base_dir")]
    pub base_dir: String,
    /// block of codec and all its variantes
    pub codec_list: Vec<CodecGroup>,
}

fn default_keyframe_interval() -> u32 {
    2
}

fn default_segment_duration() -> u32 {
    6
}

fn default_dash_base_dir() -> String {
    "./dash_hls".to_string()
}

#[derive(JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct CodecGroup {
    pub codec: Codec,
    pub variantes: Vec<Variant>,
}

/// this entries provide stream forwarding and local backup
/// recordings of the input stream
#[derive(JsonSchema, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Auxiliary {
    /// file path or URL
    pub path: String,
    /// container format
    #[serde(default = "default_aux_container")]
    pub container: Container,
}

fn default_aux_container() -> Container {
    Container::mp4
}

/// the used media container
#[allow(non_camel_case_types)]
#[derive(JsonSchema, Serialize, Deserialize, Debug, PartialEq)]
pub enum Container {
    mp4,
    webm,
    mov,
    null,
}

impl Default for Container {
    fn default() -> Self {
        Container::mp4
    }
}

/// the used encoder
/// here ae are mixing the lists of audio and video codecs
#[allow(non_camel_case_types)]
#[derive(JsonSchema, Serialize, Deserialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum Codec {
    VideoCodec(VideoCodec),
    AudioCodec(AudioCodec),
}

/// video codecs
#[allow(non_camel_case_types)]
#[derive(JsonSchema, Serialize, Deserialize, Debug, PartialEq)]
pub enum VideoCodec {
    libx264,
    libx265,
    libsvt_hevc,
    libsvt_av1,
    libsvt_vp9,
    vp9,
}

/// audio codecs
#[allow(non_camel_case_types)]
#[derive(JsonSchema, Serialize, Deserialize, Debug, PartialEq)]
pub enum AudioCodec {
    /// copy the audio channel without reencoding
    audio_passtrough,
    libfdk_aac,
    aac,
}

/// the different quality variants
#[derive(Default, JsonSchema, Serialize, Deserialize, Debug, PartialEq)]
#[serde(deny_unknown_fields)]
pub struct Variant {
    /// height or width of scaling
    pub width: Option<u32>,
    pub height: Option<u32>,
    /// fixed bitrate setting
    pub bitrate: Option<u32>,
    /// use constant rate factor for bitrate estimation
    ///
    /// WARNING: this doesn't work acceptable in ffmpegs
    /// dash/hls implementation, because the bitrate settings
    /// will not be set!:(
    pub crf: Option<u32>,
    /// apply r128 loudness normalization
    #[serde(default = "default_as_false")]
    pub r128: bool,
    // // this feature is temporary disabled, because all custom
    // // options would need mapping suffixes...
    //
    // additional ffmpeg configuration options
    // pub custom_ffmpeg: Option<String>,
}
