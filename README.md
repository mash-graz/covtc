# CoV Transcoder (`covtc`)

This is an auxiliary tool to handle `ffmpeg` and `gstreamer` transcoding
pipelines for HLS and DASH adaptive video delivery in a more human friendly
manner by utilizing a minimalist set of configuration instructions in YAML or
Json format.

By utilizing the well known command line tools `ffmpeg` or `gst-launch` it's in
principle already possible to prepare bundles of static DASH/HLS content for
video delivery on the web and efficient live broadcast, but it's still a rather
challenging endeavour to master the complex and error prone instruction
sequences. Most of the end users are therefore at the mercy of dedicated
internet video platforms and service providers, if they want to publish their
own creative work. This software tries to bring back nowadays video delivery
capabilities into the hands of ordinary people and artists by providing a free
software alternative to the hidden magic of processing by huge internet video
service platforms. `covtc` helps to handle the necessary content preparation and
delivery as comfortable as possible and still utilize advanced techniques and
best practice of modern video delivery (e.g. VP9 and AV1 codec support, CMAF
intermadate files and SRT protocol for the live stream uplink).

At its core it translates a sparse YAML or Json description of the processing
pipeline to the actual `ffmpeg` or `gst-launch` invocation commands. This
doesn't provide all the available features and customizable control supported by
the underlying tools, but in return it eliminates a lot of entangled and error
prone details by using a compact high level description of the actual goal. The
syntax of this kind of pipeline description is also specified by a Json Schema
to support automatic input validation, web form generation and interactive help
by various editing applications.

## How to use the `covtc`

**Warning: be warned, right now this is just a very early first draft of the application for beta testing purposes!**

Although you could simply download and run the `covtc` binary on any linux
machine as a simple stand alone command line tool, the containerized
distribution, which also includes a special ffmpeg version supporting very
efficient codecs from [intels OpenVisualCloud SVT
project](https://01.org/openvisualcloud), is the preferred mode of operation.

Utilizing [docker](https://www.docker.com/) for this purpose may look a little
bit inconvenient to users, which didn't use this kind of tools before, but it
opens a lot of useful opportunities, which we couldn't realize otherwise with
moderate efforts (e.g. platform agnostic execution on mac, windows and linux and
seamless integration into more advanced server infrastructure).

to run `covtc` within a docker environment, you simply call it like this:

```
docker run -t --rm registry.gitlab.com/mash-graz/covtc covtc --help
```

The option `--help`, used in this particular example, will display some elementary usage information:

```
covtc 0.1.0
# The Command Line Options of the application

USAGE:
    covtc [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <config>              YAML or Json config file (appropriate suffix required)
    -e, --example <example>            write a YAML example file. (use 'stdin' if used without arguments)
    -j, --json-config <json-config>    Json config string [env: COVTC_JSON_CONFIG=]
    -s, --schema <schema>              dumps the Json Schema. (use 'stdin' if used without argument)
```

For any more useful utilization of the application, you have add another
important docker option to *bind mount* a directory of your machine within the
containerized envrionment -- something like: `--volume SOURCE_DIR:TARGET_DIR`,
or shorter `-v`.

if you want to make your actual working directory accessible under `/tmp`, this
will look like this:

```
docker run -t --rm -v $(pwd):/tmp registry.gitlab.com/mash-graz/covtc covtc ...
```

Don't worry, this is the only rather complicated obstace, if you are new to
containerized work. ;) But now you can finally use the tool for its actual
purpose -- i.e. transcoding video files resp. preparing DASH/HLS video content
for web delivery.

Because transcoding instructions always tend to be very complex in practice,
`covtc` supports two ways to specify the operation:

* you can use a Json string on the command line -- an option, which is in fact
  intended to be used by automized machine interaction on the server side.

* for more handy human interaction, you should use a YAML configuration file
  describing the transcodeing pipeline.

The YAML configuration syntax looks like this:

```ỳaml
---
input:
  inputfile:
    path: /tmp/video_file.mp4
output:
  dash_hls:
    base_dir: /tmp/covtc_output
    codec_list:
      - codec: libx264
        variantes:
            - height:  1080
              bitrate: 6000000
            - height:  720
              bitrate: 4000000
            - height:  560
              bitrate: 2000000
            - height:  432
              bitrate: 800000
            - height:  360
              bitrate: 400000
      - codec: vp9
        variantes:
            - height:  1080
              bitrate: 4000000
            - height:  720
              bitrate: 3000000
            - height:  560
              bitrate: 1200000
            - height:  432
              bitrate: 300000
            - height:  360
              bitrate: 150000
      - codec: libfdk_aac
        variantes:
          - bitrate: 192000
            r128: true
          - bitrate: 128000
            r128: true
```

If a file like this is located in your present working directory close to a
video file of name `video_file.mp4`, you could use this command to generate a
new folder -- `covtc_output` as specified in the config file -- including all
the needed stuff to publish the video clip on any ordinary webserver:

```
docker run -t --rm -v $(pwd):/tmp registry.gitlab.com/mash-graz/covtc covtc -c config.yaml
```

Right now there isn't much helpful documention about this configuration options
available beside the [autogenerated source code
overview](https://mash-graz.gitlab.io/covtc/covtc/pipeline/index.html), but this
will be written over time. in fact, `covtc` already supports sone very useful
additional features (like live streaming capabilities etc.) out of the box.

### Features and TODO list:

* [X] Json Schema generation
* [X] pipeline translation for `ffmpeg`
* [ ] pipeline translation for `gst-launch` or native gstreamer support
* [ ] progress display
* [ ] testrun and benchmark support
* [X] live encoding capabilites [untested!]
  * [ ] stand alone live encoding (i.e. embedded web server)
  * [ ] monitoring (log, prometheus)
* [X] export bundles of static HLS+DASH web content in CMAF format
* [X] include preconfigured [indigo player](https://github.com/matvp91/indigo-player)
* [ ] quality metrics for the generated variants
* [X] loudness normalization option (r128)
* [ ] thumbs creation
* [ ] human friendly bitrate notation (i.e. M and K suffix)
* [ ] overriding config file entries by command line flags (i.e. template like operation)
* [ ] eliminate useless resolutions (i.e. input smaller
      than output size)
